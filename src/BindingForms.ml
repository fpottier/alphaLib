(* This module merges the visitor classes defined in the following modules:

   - [BindingFormsAbs], which provides abstractions of one name in one term;

   - [BindingCombinators], which provides a combinator language for defining
     complex binding constructs.

   This module also defines dummy [visit_'bn] methods, which are never called
   but must exist (because we expect the user-defined type of terms to be
   parameterized over ['bn] and ['fn]). *)

(* -------------------------------------------------------------------------- *)

(* Import the type definitions in [BindingFormsAbs]. *)

type ('bn, 't) abs =
  ('bn, 't) BindingFormsAbs.abs

(* Import the type definitions in [BindingCombinators]. *)

type 'p abstraction =
  'p BindingCombinators.abstraction

type 'bn binder =
  'bn BindingCombinators.binder

type 't inner =
  't BindingCombinators.inner

type 't outer =
  't BindingCombinators.outer

type 'p rebind =
  'p BindingCombinators.rebind

type ('p, 't) bind =
  ('p, 't) BindingCombinators.bind

(* -------------------------------------------------------------------------- *)

(* The empty type [void] is used in the definition of the dummy methods
   [visit_'bn] below. This allows us to statically ensure that these
   methods are never called. *)

type void

(* -------------------------------------------------------------------------- *)

(* [iter] *)

class virtual ['self] iter = object (_ : 'self)

  method private visit_'bn: void -> void -> void
  = fun _ _ -> assert false

  inherit [_] BindingFormsAbs.iter
  inherit [_] BindingCombinators.iter

end

(* -------------------------------------------------------------------------- *)

(* [iter2] *)

class virtual ['self] iter2 = object (_ : 'self)

  method private visit_'bn: void -> void -> void -> void
  = fun _ _ _ -> assert false

  inherit [_] BindingFormsAbs.iter2
  inherit [_] BindingCombinators.iter2

end

(* -------------------------------------------------------------------------- *)

(* [map] *)

class virtual ['self] map = object (_ : 'self)

  method private visit_'bn: void -> void -> void
  = fun _ _ -> assert false

  inherit [_] BindingFormsAbs.map
  inherit [_] BindingCombinators.map

end

(* -------------------------------------------------------------------------- *)

(* [endo] *)

class virtual ['self] endo = object (_ : 'self)

  method private visit_'bn: void -> void -> void
  = fun _ _ -> assert false

  inherit [_] BindingFormsAbs.endo
  inherit [_] BindingCombinators.map (* intentional *)

end
