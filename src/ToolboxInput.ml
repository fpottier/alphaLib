module type INPUT = sig

  (* Suppose there is a type of terms, which is parameterized over the
     representations of binding name occurrences and free name occurrences. *)

  type ('bn, 'fn) term

  (* Suppose the type of terms is equipped with the following visitors. *)

  (* The private virtual method [visit_'fn] is used to specify what should
     be done at free name occurrences. The private virtual method [extend]
     is used to indicate how the environment should be extended when an
     abstraction is entered. *)

  (* Suppose the data constructor for variables is named [TVar], so that
     the method [visit_TVar] is used to specify what behavior at variables
     is desired. *)

  class virtual ['self] iter : object ('self)
    method private virtual extend : 'env -> 'bn -> 'env
    method private virtual visit_'fn : 'env -> 'fn -> _
    method visit_term : 'env -> ('bn, 'fn) term -> unit
  end

  class virtual ['self] map : object ('self)
    method private virtual extend : 'env -> 'bn1 -> 'env * 'bn2
    method private virtual visit_'fn : 'env -> 'fn1 -> 'fn2
    method visit_term : 'env -> ('bn1, 'fn1) term -> ('bn2, 'fn2) term
    method private visit_TVar : 'env -> 'fn1 -> ('bn2, 'fn2) term
  end

  class virtual ['self] endo : object ('self)
    method private virtual extend : 'env -> 'bn -> 'env * 'bn
    method private virtual visit_'fn : 'env -> 'fn -> 'fn
    method visit_term : 'env -> ('bn, 'fn) term -> ('bn, 'fn) term
    method private visit_TVar : 'env -> ('bn, 'fn) term -> 'fn -> ('bn, 'fn) term
  end

  class virtual ['self] iter2 : object ('self)
    method private virtual extend : 'env -> 'bn1 -> 'bn2 -> 'env
    method private virtual visit_'fn : 'env -> 'fn1 -> 'fn2 -> _
    method visit_term : 'env -> ('bn1, 'fn1) term -> ('bn2, 'fn2) term -> unit
  end

end
