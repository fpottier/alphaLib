(* This is a trivial kit. *)

(* At an abstraction or at a name occurrence, nothing special happens. *)

(* The type of the environment is undetermined. *)

class ['self] iter = object (_ : 'self)
  method private extend env _x = env
  method private visit_'fn _env _x = ()
end
