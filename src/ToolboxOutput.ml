module type OUTPUT = sig

  (* A type of terms, is parameterized over the representations of binding
     name occurrences and free name occurrences. *)

  type ('bn, 'fn) term

  (* A raw term is one where every name is represented as a string. This form
     is typically produced by a parser, and consumed by a printer. It is not
     used internally. *)

  type raw_term =
    (string, string) term

  (* A nominal term is one where every name is represented as an atom. Although
     this is not visible in this type definition, we may additionally impose a
     Global Uniqueness Hypothesis (GUH), that is, we may require every binding
     name occurrence to carry a distinct atom. *)

  type nominal_term =
    (Atom.t, Atom.t) term

  (* [fa_term] computes the set of the free atoms of a term. *)

  val fa_term: nominal_term -> Atom.Set.t

  (* [filter_term p t] returns a free atom of the term [t] that satisfies the
     predicate [p], if such an atom exists. *)

  val filter_term: (Atom.t -> bool) -> nominal_term -> Atom.t option

  (* [closed_term t] tests whether the term [t] is closed, that is, whether
     [t] has no free atom. *)

  val closed_term: nominal_term -> bool

  (* [occurs_term x t] tests whether the atom [x] occurs free in the term [t]. *)

  val occurs_term: Atom.t -> nominal_term -> bool

  (* [ba_term] computes the set of bound atoms of a term. *)

  val ba_term: nominal_term -> Atom.Set.t

  (* [avoids_term env t] tests whether the bound atoms of [t] avoid the set [env],
     that is, [ba(t) # env]. It also checks that there is no shadowing within [t],
     that is, no atom is bound twice along a branch. It returns [true] if these
     two conditions are satisfied. *)

  val avoids_term: Atom.Set.t -> nominal_term -> bool

  (* [guq_term] tests whether a term satisfies global uniqueness, that is, no atom
     is bound twice within this term (not even along different branches). *)

  (* [guq_terms] checks that a list of terms satisfies global uniqueness, that is,
     no atom is bound twice within this list (not even within two distinct list
     elements). *)

  (* [guq_term] and [guq_terms] should be used only in debugging mode, typically
     in an [assert] construct. They print the identity of one offending atom on
     the standard error channel. *)

  val guq_term: nominal_term -> bool
  val guq_terms: nominal_term list -> bool

  (* [copy_term] returns a copy of its argument where every bound name has been
     replaced with a fresh copy, and every free name is unchanged. *)

  val copy_term: nominal_term -> nominal_term

  (* [avoid_term bad] returns a copy of its argument where some bound names have
     been replaced with a fresh copy, so as to ensure that no bound name is in
     the set [bad]. *)

  val avoid_term: Atom.Set.t -> nominal_term -> nominal_term

  (* [import_term] converts a raw term to a nominal term that satisfies the
     Global Uniqueness Hypothesis, that is, a nominal term where every binding
     name occurrence is represented by a unique atom. [import] expects every
     free name occurrence to be in the domain of [env]. If that is not the case,
     the exception [Unbound] is raised. *)

  exception Unbound of string
  val import_term: KitImport.env -> raw_term -> nominal_term

  (* [export_term] converts a nominal term to a raw term, in a hygienic manner.
     This is the proper way of displaying a term. [export] expects every free
     name occurrence to be in the domain of [env]. *)

  val export_term: KitExport.env -> nominal_term -> raw_term

  (* [show_term] converts its argument to a raw term using [Atom.show] both at
     free name occurrences and bound name occurrences. It is in principle a
     debugging tool only, as the strings produced by [Atom.show] do not make
     sense (at free atoms) and are not beautiful. It is nevertheless a correct
     printer IF the term is closed. *)

  val show_term: nominal_term -> raw_term

  (* [size_term] computes the size of its argument. Beware: this counts the
     nodes whose type is [term], but does not count the nodes of other types. *)

  val size_term: (_, _) term -> int

  (* [equiv_term] tests whether two terms are alpha-equivalent. *)

  val equiv_term: nominal_term -> nominal_term -> bool

  (* [rename_term] applies a renaming to the free atoms of a nominal term,
     yielding a nominal term. *)

  val rename_term: Atom.renaming -> nominal_term -> nominal_term

  (* [subst_TVar_term] applies a substitution to a nominal term, yielding a
     nominal term. This is a substitution of things for variables; in the
     general case, [TVar] could be a constructor of some type [thing] other
     than [term]. *)

  (* A substitution is a finite map of atoms to things. *)

  (* When applying a substitution [sigma] to a term [t], the precondition of this
     operation is that the bound atoms of [t] are fresh for [sigma], that is, do
     not appear in the domain or codomain of [sigma]. This guarantees two things:
     1- the free atoms in the codomain of [sigma] cannot be captured by a binder
     within [t]; and 2- an atom in the domain of [sigma] cannot be masked by a
     binder within [t], so we can go down into [t] and apply [sigma] to every
     variable. *)

  (* Global uniqueness can be preserved, if desired, by copying the things that
     are grafted into [t]. The user decides which [copy] operation should be used.
     It could be [copy_thing], or it could be the identity. *)

  val subst_TVar_term:
    (nominal_term -> nominal_term) ->
    nominal_term Atom.Map.t -> nominal_term -> nominal_term

  (* [subst_TVar_term1] applies a singleton substitution to a nominal term,
     yielding a nominal term. *)

  val subst_TVar_term1:
    (nominal_term -> nominal_term) ->
    nominal_term  -> Atom.t -> nominal_term -> nominal_term

end
