(* This kit serves to construct a [copy] function for terms. *)

(* An environment maps atoms to atoms. *)

type env =
  Atom.atom Atom.Map.t

let empty =
  Atom.Map.empty

let lookup env x =
  try
    Atom.Map.find x env
  with Not_found ->
    (* Outside of its domain, the renaming acts as the identity. *)
    x

let extend env x =
  (* Generate a fresh copy of [x]. *)
  let x' = Atom.copy x in
  (* Extend [env] when descending in the body. *)
  Atom.Map.add x x' env, x'

class ['self] map = object (_ : 'self)
  method private extend = extend
  method private lookup = lookup
  method private visit_'fn = lookup
end
