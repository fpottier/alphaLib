(* This kit serves to check that a term satisfies ``global uniqueness'', that
   is, no atom is bound twice inside this term (not even along distinct
   branches). The exception [Atom.Set.NonDisjointUnion x] is raised if the
   atom [x] occurs twice in a binding position. *)

class ['self] iter = object (_ : 'self)

  val mutable accu = Atom.Set.empty

  (* A bound atom is added to the accumulator when its scope is entered. *)
  method private extend () x =
    if Atom.Set.mem x accu then
      raise (Atom.Set.NonDisjointUnion x)
    else
      accu <- Atom.Set.add x accu

  method private visit_'fn () _x =
    ()

end
