open ToolboxInput

(* This functor is applied to a type of terms, equipped with visitor classes.
   It produces a toolbox of useful functions that operate on terms. *)

module Make (Term : INPUT) = struct

  include Term

(* -------------------------------------------------------------------------- *)

  (* A raw term is one where every name is represented as a string. This form
     is typically produced by a parser, and consumed by a printer. It is not
     used internally. *)

  type raw_term =
    (string, string) term

  (* A nominal term is one where every name is represented as an atom. Although
     this is not visible in this type definition, we may additionally impose a
     Global Uniqueness Hypothesis (GUH), that is, we may require every binding
     name occurrence to carry a distinct atom. *)

  type nominal_term =
    (Atom.t, Atom.t) term

(* -------------------------------------------------------------------------- *)

  (* All of the code is produced by macro-expansion. *)

  (* This serves as a test of our macros, which can also be used directly by
     the end user, in situations where the functor [Toolbox.Make] cannot be
     used. *)

  #include "AlphaLibMacros.cppo.ml"

  __ALL
  ALL(term)

  (* Mnemonic: Substitute for variables in terms. *)
  __SUBST(TVar)
  SUBST(TVar, term)

(* -------------------------------------------------------------------------- *)

end
