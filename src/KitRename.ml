(* This kit serves to construct a [rename] function for terms -- a function
   that substitutes atoms for atoms. *)

(* An environment is a map of atoms to atoms. We require every binder [x]
   encountered along the way to be fresh with respect to [env]. *)

type env =
  Atom.t Atom.Map.t

let extend env x =
  (* We would like to check that [x] is fresh for [env], but can only
     perform the domain check. The codomain check cannot be performed
     since the type of things is abstract here. *)
  assert (not (Atom.Map.mem x env));
  (* Since [x] is fresh for [env], no capture is possible. Thus, no
     freshening of the bound name is required. Thus, we can keep the
     substitution [env], unchanged, under the binder. *)
  env, x

let lookup env x =
  try
    Atom.Map.find x env
  with Not_found ->
    x

class ['self] map = object (_ : 'self)
  method private extend = extend
  method private lookup = lookup
  method private visit_'fn = lookup
end
