(* -------------------------------------------------------------------------- *)

(* Computing the free atoms of a term, via [reduce]. *)

(* In this style, no environment is required. *)

(* type env = unit *)

class ['self] reduce = object (_ : 'self)

  (* The monoid of sets of atoms is used. *)
  inherit [_] Atom.Set.union_monoid

  method private extend _x () = ()

  (* The atom [x] is removed from the set of free atoms when the scope of [x]
     is exited. *)
  method private restrict = Atom.Set.remove

  method private visit_'fn () x = Atom.Set.singleton x

end

(* -------------------------------------------------------------------------- *)

#define __FA                                                                   \
  class FA_CLASS = object                                                      \
    inherit [_] reduce                                                         \
    inherit [_] KitFa.reduce                                                   \
  end                                                                          \

#define FA(term)                                                               \
  let FA_FUN(term) t =                                                         \
    new FA_CLASS # VISIT(term) () t                                            \
