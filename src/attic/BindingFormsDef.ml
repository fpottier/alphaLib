(* -------------------------------------------------------------------------- *)

(* A definition is a binding form of the form [let (rec) x = t in u]. The name
   [x] is in scope in [u]. It is in scope in [t] if the definition is marked
   recursive; otherwise, it is not. The terms [t] and [u] need not belong to the
   same syntactic category. *)

type recursive =
  | NonRecursive
  | Recursive

type ('bn, 't, 'u) def =
  recursive * 'bn * 't * 'u

let choose recursive env env' =
  match recursive with
  | NonRecursive ->
      env
  | Recursive ->
      env'

class virtual ['self] map = object (self : 'self)

  method private visit_def: 't1 't2 'u1 'u2 .
    _ ->
    ('env -> 't1 -> 't2) ->
    ('env -> 'u1 -> 'u2) ->
    'env -> ('bn1, 't1, 'u1) def -> ('bn2, 't2, 'u2) def
  = fun _ visit_t visit_u env (recursive, x1, t1, u1) ->
      let x2, env' = self#extend x1 env in
      let t2 = visit_t (choose recursive env env') t1 in
      let u2 = visit_u env' u1 in
      recursive, x2, t2, u2

end

class virtual ['self] endo = object (self : 'self)

  method private visit_def: 't 'u .
    _ ->
    ('env -> 't -> 't) ->
    ('env -> 'u -> 'u) ->
    'env -> ('bn, 't, 'u) def -> ('bn, 't, 'u) def
  = fun _ visit_t visit_u env ((recursive, x1, t1, u1) as this) ->
      let x2, env' = self#extend x1 env in
      let t2 = visit_t (choose recursive env env') t1 in
      let u2 = visit_u env' u1 in
      if x1 == x2 && t1 == t2 && u1 == u2 then
        this
      else
        recursive, x2, t2, u2

end

class virtual ['self] iter = object (self : 'self)

  method private visit_def: 't 'u .
    _ ->
    ('env -> 't -> unit) ->
    ('env -> 'u -> unit) ->
    'env -> ('bn, 't, 'u) def -> unit
  = fun _ visit_t visit_u env (recursive, x, t, u) ->
      let env' = self#extend x env in
      visit_t (choose recursive env env') t;
      visit_u env' u

end

class virtual ['self] map2 = object (self : 'self)

  method private visit_def: 't1 'u1 't2 'u2 't3 'u3 .
    _ ->
    ('env -> 't1 -> 't2 -> 't3) ->
    ('env -> 'u1 -> 'u2 -> 'u3) ->
    'env -> ('bn1, 't1, 'u1) def -> ('bn2, 't2, 'u2) def -> ('bn3, 't3, 'u3) def
  = fun _ visit_t visit_u env (recursive1, x1, t1, u1) (recursive2, x2, t2, u2) ->
      if recursive1 <> recursive2 then VisitorsRuntime.fail();
      let x3, env' = self#extend x1 x2 env in
      let t3 = visit_t (choose recursive1 env env') t1 t2 in
      let u3 = visit_u env' u1 u2 in
      recursive1, x3, t3, u3

end

class virtual ['self] reduce2 = object (self : 'self)

  method private visit_def: 't1 'u1 't2 'u2 .
    _ ->
    ('env -> 't1 -> 't2 -> 'z) ->
    ('env -> 'u1 -> 'u2 -> 'z) ->
    'env -> ('bn1, 't1, 'u1) def -> ('bn2, 't2, 'u2) def -> 'z
  = fun _ visit_t visit_u env (recursive1, x1, t1, u1) (recursive2, x2, t2, u2) ->
      if recursive1 <> recursive2 then VisitorsRuntime.fail();
      let env' = self#extend x1 x2 env in
      let zt = visit_t (choose recursive1 env env') t1 t2 in
      let zu = self#restrict x1 x2 (visit_u env' u1 u2) in
      self#plus zt zu

end
