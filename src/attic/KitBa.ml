(* -------------------------------------------------------------------------- *)

(* Computing the bound atoms of a term, via [reduce]. *)

class ['self] reduce = object (_ : 'self)

  method private extend _x () =
    ()

  method private visit_'fn () _x =
    Atom.Set.empty

  (* The monoid of sets of atoms is used. *)
  inherit [_] Atom.Set.union_monoid

  (* An atom is added to the set of bound atoms when its scope is exited. *)
  method private restrict x xs =
    Atom.Set.add x xs

end

(* -------------------------------------------------------------------------- *)

#define __BA                                                                   \
  class ['self] BA_CLASS = object (_ : 'self)                                  \
    inherit [_] reduce                                                         \
    inherit [_] KitBa.reduce                                                   \
  end                                                                          \

#define BA(term)                                                               \
  let BA_FUN(term) t =                                                         \
    new BA_CLASS # VISIT(term) () t                                            \
