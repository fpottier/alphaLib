(* -------------------------------------------------------------------------- *)

(* A universal, concrete type of single-name abstractions. *)

(* We wish to represent all kinds of abstractions -- e.g. in nominal style,
   in de Bruijn style, etc. -- so we parameterize the abstraction over the
   type ['bn] of the bound name and over the type ['term] of the body. This
   makes this type definition almost trivial -- it is just a pair -- but it
   still serves as a syntactic marker of where abstractions are located. *)

type ('bn, 'term) abs =
  'bn * 'term

(* -------------------------------------------------------------------------- *)

(* The main effect of a binding construct is to cause the environment to be
   enriched when the abstraction is traversed. The following visitor methods
   define where the environment is enriched. *)

(* These methods do not know the type of the environment, and do not know how
   it is enriched; the latter task is delegated to the virtual method
   [extend]. The implementation of this method is provided by separate
   ``kits''. *)

(* We need several varieties of visitors, which is a bit painful. As of now,
   [iter], [map], [endo], [iter2] are required: see [ToolboxInput]. *)

(* The visitor methods are polymorphic in the type of terms. This is
   important, as it means that one can use several instances of a binding
   construct in a single type definition and still be able to construct
   well-typed visitors. *)

(* The virtual method [extend] is not polymorphic in the types of bound names
   and environments. On the contrary, each kit comes with certain specific
   types of bound names and environments. *)

(* -------------------------------------------------------------------------- *)

(* [iter] *)

class virtual ['self] iter = object (self : 'self)

  method private virtual extend: 'env -> 'bn -> 'env

  method private visit_abs: 'term .
    _ ->
    ('env -> 'term -> unit) ->
    'env -> ('bn, 'term) abs -> unit
  = fun _ visit_term env (x, t) ->
      let env' = self#extend env x in
      visit_term env' t

end

(* -------------------------------------------------------------------------- *)

(* [iter2] *)

class virtual ['self] iter2 = object (self : 'self)

  method private virtual extend: 'env -> 'bn1 -> 'bn2 -> 'env

  method private visit_abs: 'term1 'term2 .
    _ ->
    ('env -> 'term1 -> 'term2 -> 'z) ->
    'env -> ('bn1, 'term1) abs -> ('bn2, 'term2) abs -> 'z
  = fun _ visit_term env (x1, t1) (x2, t2) ->
      let env' = self#extend env x1 x2 in
      visit_term env' t1 t2

end

(* -------------------------------------------------------------------------- *)

(* [map] *)

class virtual ['self] map = object (self : 'self)

  method private virtual extend: 'env -> 'bn1 -> 'env * 'bn2

  method private visit_abs: 'term1 'term2 .
    _ ->
    ('env -> 'term1 -> 'term2) ->
    'env -> ('bn1, 'term1) abs -> ('bn2, 'term2) abs
  = fun _ visit_term env (x1, t1) ->
      let env, x2 = self#extend env x1 in
      let t2 = visit_term env t1 in
      x2, t2

end

(* -------------------------------------------------------------------------- *)

(* [endo] *)

class virtual ['self] endo = object (self : 'self)

  method private virtual extend: 'env -> 'bn1 -> 'env * 'bn2

  method private visit_abs: 'term .
    _ ->
    ('env -> 'term -> 'term) ->
    'env -> ('bn, 'term) abs -> ('bn, 'term) abs
  = fun _ visit_term env ((x1, t1) as this) ->
      let env, x2 = self#extend env x1 in
      let t2 = visit_term env t1 in
      if x1 == x2 && t1 == t2 then
        this
      else
        x2, t2

end
