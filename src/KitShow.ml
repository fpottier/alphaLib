(* This kit serves to construct a [show] function for terms. *)

(* At an abstraction or at a name occurrence, [Atom.show] is applied. *)

let lookup _env x =
  Atom.show x

class ['self] map = object (_ : 'self)
  method private extend env x = env, Atom.show x
  method private lookup = lookup
  method private visit_'fn = lookup
end
