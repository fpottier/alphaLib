(* This kit serves to construct an [import] function for terms, that is, a
   function that transforms strings to atoms. *)

(* We map every binding occurrence to a fresh atom, so the term that we
   produce satisfies global uniqueness. *)

module StringMap =
  Map.Make(String)

type env =
  Atom.t StringMap.t

let empty =
  StringMap.empty

let extend (env : env) (x : string) : env * Atom.t =
  let a = Atom.fresh x in
  let env = StringMap.add x a env in
  env, a

exception Unbound of string

let lookup (env : env) (x : string) : Atom.t =
  try
    StringMap.find x env
  with Not_found ->
    raise (Unbound x)

class ['self] map = object (_ : 'self)
  method private extend = extend
  method private lookup = lookup
  method private visit_'fn = lookup
end
