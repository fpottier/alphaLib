(* This kit serves to compute the set of ``bound atoms'' of a term, that is,
   the set of all binding name occurrences. *)

class ['self] iter = object (_ : 'self)

  val mutable accu = Atom.Set.empty

  method accu = accu (* must be public *)

  (* A bound atom is added to the accumulator when its scope is entered. *)
  method private extend () x =
    accu <- Atom.Set.add x accu

  method private visit_'fn () _x =
    ()

end
