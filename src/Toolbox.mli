open ToolboxInput
open ToolboxOutput

(* This functor is applied to a type of terms, equipped with visitor classes.
   It produces a toolbox of useful functions that operate on terms. *)

module Make (Term : INPUT) : OUTPUT
  with type ('bn, 'fn) term = ('bn, 'fn) Term.term
