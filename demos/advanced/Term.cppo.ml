open AlphaLib
open BindingForms

#define tele ('bn, 'fn) tele
#define term ('bn, 'fn) term

type tele =
  | TeleNil
  | TeleCons of 'bn binder * term outer * tele rebind

and term =
  | TVar of 'fn
  | TPi  of (tele, term) bind
  | TLam of (tele, term) bind
  | TApp of term * term list
[@@deriving visitors { variety = "iter"; ancestors = ["BindingForms.iter"]; public = ["visit_term"] },
            visitors { variety = "map"; ancestors = ["BindingForms.map"]; public = ["visit_term"] },
            visitors { variety = "endo"; ancestors = ["BindingForms.endo"]; public = ["visit_term"] },
            visitors { variety = "iter2"; ancestors = ["BindingForms.iter2"]; public = ["visit_term"] }]
