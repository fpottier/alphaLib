open AlphaLib
open BindingForms

type ('bn, 'fn) term =
  | TVar of 'fn
  | TLambda of ('bn, ('bn, 'fn) term) abs
  | TApp of ('bn, 'fn) term * ('bn, 'fn) term

  [@@deriving

    visitors { variety = "iter"; public = ["visit_term"];
               ancestors = ["BindingForms.iter"] }
    ,
    visitors { variety = "map"; public = ["visit_term"];
               ancestors = ["BindingForms.map"] }
    ,
    visitors { variety = "endo"; public = ["visit_term"];
               ancestors = ["BindingForms.endo"] }
    ,
    visitors { variety = "iter2"; public = ["visit_term"];
               ancestors = ["BindingForms.iter2"] }

  ]
