(* Printers for types and terms. *)

val typ: out_channel -> F.raw_typ -> unit
val term: out_channel -> F.raw_term -> unit
