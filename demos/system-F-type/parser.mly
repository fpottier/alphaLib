%token ARROW
%token COLON
%token COMMA
%token DOT
%token STAR /* For product */
%token SEMICOLON

%token LPARENT
%token RPARENT

%token LSQUAREBRACKET
%token RSQUAREBRACKET

%token EQUAL

%token FORALL

%token LET
%token IN

%token <string> TYPEVAR
%token <string> VAR

%token <int> PROJECTION

%token ABSTRACTION /* small lambda */
%token TYPEABSTRACTION /* capital lambda */

%token EOF

%start <F.raw_term> top_level
%%

(* A top level is a term. See that only terms are accepted as top level
   expression. Type term like type application is not allowed.
   Parentheses are not mandatory for the top level expressions.
   A top level expression must be ended with ;; to evaluate it (as in OCaml).
*)
top_level:
| t = term ; SEMICOLON ; SEMICOLON { t }
| EOF { raise End_of_file }

term:
(* Simple variable *)
| id = VAR { F.TeVar(id) }
(* Term abstraction *)
| ABSTRACTION ;
  bv = VAR ;
  COLON ;
  typevar = type_term ;
  DOT ;
  t = term {
    F.TeAbs(bv, typevar, t)
  }
(* Build an abstraction. For ease of reading when the variable type is long,
  parentheses are added around the variable and the type.
*)
| ABSTRACTION ;
  LPARENT ;
  bv = VAR ;
  COLON ;
  typevar = type_term ;
  RPARENT ;
  DOT ;
  t = term {
    F.TeAbs(bv, typevar, t)
  }
(* Build a type application *)
| t1 = term ;
  LSQUAREBRACKET ;
  t2 = type_term ;
  RSQUAREBRACKET {
    F.TeTyApp(t1, t2)
  }
(* Build a pair *)
| LPARENT ;
  t1 = term ;
  COMMA ;
  t2 = term ;
  RPARENT {
    F.TePair(t1, t2)
  }
(* Build a projection *)
| i = PROJECTION ;
  t = term {
    F.TeProj(i, t)
  }
(* Build a term application. No parentheses are mandatory.
 *)
|
  t1 = term ;
  t2 = term {
    F.TeApp(t1, t2)
  }
(* Build a Let definition *)
| LET ;
  bv = VAR ;
  EQUAL ;
  t1 = term ;
  IN ;
  t2 = term {
    F.TeLet(bv, t1, t2)
  }
(* Type abstraction *)
| TYPEABSTRACTION ;
  id = TYPEVAR ;
  DOT ;
  t = term {
    F.TeTyAbs(id, t)
  }
(* Surround term with nested parentheses. Allow to add an infinite number of
   parentheses.
*)
| LPARENT ;
  t = term ;
  RPARENT {
    t
  }

type_term:
| id = TYPEVAR { F.TyVar(id) }
| ty1 = type_term ; STAR ; ty2 = type_term { F.TyProduct(ty1, ty2) }
| t1 = type_term ; ARROW ; t2 = type_term { F.TyArrow(t1, t2) }
| FORALL ; id = TYPEVAR ; DOT ; t = type_term { F.TyForall(id, t) }
| LPARENT ; t = type_term ; RPARENT { t }
