{
  open Lexing
  let next_line lexbuf =
    let pos = lexbuf.lex_curr_p in
    lexbuf.lex_curr_p <-
      {
        pos with pos_bol = lexbuf.Lexing.lex_curr_pos;
                 pos_lnum = pos.Lexing.pos_lnum + 1
      }


}

let abstraction = "\\"
let type_abstraction = "\\\\"
let for_all = "forall"
let projection = "proj" ['0'-'9']+

let white = [' ' '\t' '\r']
let newline = ['\n']

rule prog = parse
| white { prog lexbuf }
| newline { next_line lexbuf; prog lexbuf }
| ',' { Parser.COMMA }
| abstraction { Parser.ABSTRACTION }
| type_abstraction { Parser.TYPEABSTRACTION }
| "let" { Parser.LET }
| "in" { Parser.IN }
| for_all { Parser.FORALL }
| '[' { Parser.LSQUAREBRACKET }
| ']' { Parser.RSQUAREBRACKET }
| '(' { Parser.LPARENT }
| ')' { Parser.RPARENT }
| '=' { Parser.EQUAL }
| ':' { Parser.COLON }
| ';' { Parser.SEMICOLON }
| '.' { Parser.DOT }
| '*' { Parser.STAR }
| "->" { Parser.ARROW }
| projection as l {
    let len = String.length l in
    Parser.PROJECTION(int_of_string @@ String.sub l 4 (len - 4))
  }
| ['a'-'z']+ as id {
    Parser.VAR(id)
  }
| ['A'-'Z']+ ['a'-'z' 'A'-'Z']* as id {
    Parser.TYPEVAR(id)
  }
| _ { failwith "Illegal character" }
| eof { Parser.EOF}
