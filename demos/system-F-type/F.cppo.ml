open AlphaLib
open BindingForms

(* In this demo, only type variables are handled via AlphaLib. Term variables
   are represented as strings. *)

(* Type variables. *)

type tyvar =
  Atom.t

(* Types. *)

#define typ ('bn, 'fn) typ
#define term ('bn, 'fn) term

type typ =
  | TyVar of 'fn
  | TyArrow of typ * typ
  | TyProduct of typ * typ
  | TyForall of ('bn, typ) abs

(* Term variables. *)

and tevar = (string[@opaque])

(* Terms. *)

and term =
  | TeVar of tevar
  | TeAbs of tevar * typ * term
  | TeApp of term * term
  | TeLet of tevar * term * term
  | TeTyAbs of ('bn, term) abs
  | TeTyApp of term * typ
  | TePair of term * term
  | TeProj of int * term

#undef typ
#undef term

(* Visitor generation. *)

[@@deriving
  visitors { variety = "iter";   ancestors = ["BindingForms.iter"] },
  visitors { variety = "map";    ancestors = ["BindingForms.map"]  },
  visitors { variety = "endo";   ancestors = ["BindingForms.endo"] },
  visitors { variety = "iter2";  ancestors = ["BindingForms.iter2"] } ]

(* Type abbreviations. *)

type raw_typ =
  (string, string) typ
type nominal_typ =
  (Atom.t, Atom.t) typ

type raw_term =
  (string, string) term
type nominal_term =
  (Atom.t, Atom.t) term

(* Operations based on visitors. *)

#include "AlphaLibMacros.cppo.ml"

__ALL
ALL(typ)
ALL(term)

__SUBST(TyVar)
SUBST(TyVar, typ)
SUBST(TyVar, term)
